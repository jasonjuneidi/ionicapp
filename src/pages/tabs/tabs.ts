import { Component } from '@angular/core';

import { HomePage } from '../home/home'
import { NearbyPage } from '../nearby/nearby'
import { TimelinePage } from '../timeline/timeline'
import { DashboardPage } from '../dashborad/dashboard'

@Component({
    templateUrl: 'tabs.html'
  })
  export class TabsPage {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    tab1Root = DashboardPage;
    tab2Root = TimelinePage;
    tab3Root = NearbyPage;
    tab4Root = HomePage
  
    constructor() {
  
    }
  }